import { useRouter } from 'next/router';
import React, { useState, useEffect } from 'react';

export default function Register() {
    const [options, setOptions] = useState([]);
    const [subdomain, setSubdomain] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [isCreated, setIsCreated] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const router = useRouter();

    useEffect(() => {
        // fetch options from BE

        fetchOptions()
    },  [])

    const fetchOptions = async () => {
        try {
            const response = await fetch('http://localhost:3111/v1/portal/partners');
            const json = await response.json();
            setOptions(json.data)
        } catch (error) {
            console.error('Error fetching options:', error);
        }
    }

    const handleOptionChange = (event) => {
        console.log("subdomain: ", event.target.value)
        setSubdomain(event.target.value);
      };

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const handleNameChange = (event) => {
        setName(event.target.value);
    };

    const isFormValid = () => {
        console.log("lay: ", name, email)
        return name.trim() !== '' && email.trim() !== '' && password.trim() !== '' && subdomain.trim() !== '';
      };

    const handleSubmit = (event) => {
        console.log("Entered here");
        if(!isFormValid()) return alert("Fill all values please!")
        event.preventDefault();
        sendToBackend();
    };

    const handleModal = (event) => {
        event.preventDefault();
        if (isCreated) router.push('/api/auth/login')
        else {
            setIsCreated(false);
            setErrorMessage('')
        }
    };
    
    
    const sendToBackend = () => {
    // Make an API call to send the selected value to the backend
    // Replace 'YOUR_API_ENDPOINT' with the actual endpoint URL
        // fetch('http://localhost:3111/v1/portal/users', {
        //     method: 'POST',
        //     body: JSON.stringify({ partnerSubdomain: subdomain, email, password, name }),
        //     headers: {
        //     'Content-Type': 'application/json',
        //     },
        // })
        // .then((response) => response.json())
        // .then((data) => {
        //     // Handle the response from the backend if needed
        //     console.log('Response from backend:', data);
        //     if (data.status === "error") {
        //         setErrorMessage(data.error.message ?? data.message);
        //         setIsCreated(false);
        //     } else setIsCreated(true);
        // })
        // .catch((error) => {
        //     console.error('Error sending data to backend:', error);
        // });
    };

    if (isCreated || errorMessage.length > 0) {
        return (
            <div className="relative z-10" aria-labelledby="modal-title" role="dialog" aria-modal="true">
            <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

            <div className="fixed inset-0 z-10 overflow-y-auto">
                <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                <div className="relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg">
                    <div className="bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
                    <div className="sm:flex sm:items-start">
                        <div className="mx-auto flex h-12 w-12 flex-shrink-0 items-center justify-center rounded-full bg-green-100 sm:mx-0 sm:h-10 sm:w-10">
                            {
                                errorMessage.length === 0 ? 
                                    <svg className="h-6 w-6 text-green-600" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true"> 
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M5 13l4 4L19 7" /> 
                                    </svg> 
                                : 
                                <svg className="h-6 w-6 text-red-600" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true">
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v3.75m-9.303 3.376c-.866 1.5.217 3.374 1.948 3.374h14.71c1.73 0 2.813-1.874 1.948-3.374L13.949 3.378c-.866-1.5-3.032-1.5-3.898 0L2.697 16.126zM12 15.75h.007v.008H12v-.008z" />
                                </svg>
                            }
                        </div>
                        <div className="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left">
                        <h3 className="text-base font-semibold leading-6 text-gray-900" id="modal-title">{ errorMessage.length === 0 ? "Account Created Successfully" : "Account Creation Failed" }</h3>
                        <div className="mt-2">
                            <p className="text-sm text-gray-500">{ errorMessage.length === 0 ? "An email has been sent to the user to notify them of their account creation. They will be prompted to change their password." : errorMessage }</p>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="bg-gray-50 px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6">
                        <button onClick={handleModal} type="button" className={`inline-flex w-full justify-center rounded-md ${errorMessage.length > 0 ?  "bg-red-600" :  "bg-blue-600" } px-3 py-2 text-sm font-semibold text-white shadow-sm ${errorMessage.length > 0 ?  "hover:bg-orange-600" :  "bg-green-600" } sm:ml-3 sm:w-auto`}>
                            Close
                        </button>
                    </div>
                </div>
                </div>
            </div>
            </div>
        )
    }
    return (
        <div className="flex flex-col items-center justify-center h-screen">
            <div className="sm:mx-auto sm:w-full sm:max-w-sm">
                <img className="mx-auto h-10 w-auto" src="https://public-assets-main.s3.eu-central-1.amazonaws.com/auth0/Alteos+short+logo.png" alt="Your Company" />
                <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">Create Partner Related User</h2>
            </div>

            <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
                <form className="space-y-6" action="#" method="POST">
                    <div>
                        <label htmlFor="name" className="block text-sm font-medium leading-6 text-gray-900">Full Name</label>
                        <div className="mt-2">
                        <input onChange={handleNameChange} id="name" name="name" type="text" autoComplete="text" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
                        </div>
                    </div>
                    <div>
                        <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-900">Email address</label>
                        <div className="mt-2">
                        <input onChange={handleEmailChange} id="email" name="email" type="email" autoComplete="email" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center justify-between">
                        <label htmlFor="password" className="block text-sm font-medium leading-6 text-gray-900">Password</label>
                        </div>
                        <div className="mt-2">
                        <input onChange={handlePasswordChange} id="password" name="password" type="password" autoComplete="current-password" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center justify-between">
                        <label htmlFor="subdomain" className="block text-sm font-medium leading-6 text-gray-900">Subdomain</label>
                        </div>
                        <div className="mt-2">
                            <select value={subdomain} onChange={handleOptionChange} id="subdomain" name="subdomain" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                                <option value="">Select Partner</option>
                                {
                                    options.map((option) => (
                                        <option className='sm:text-sm sm:leading-6 text-gray-900' key={option._id} value={option.subdomain}>
                                            { option.name }
                                        </option>
                                    ))
                                }
                            </select>
                        </div> 
                    </div>

                    <div>
                        <button type="button" className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Create User</button>
                    </div>
                </form>
            </div>
        </div>
    )
}