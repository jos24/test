import React from "react";
import Link from "next/link";

// components

import Navbar from "components/Navbars/AuthNavbar.js";
import Footer from "components/Footers/Footer.js";

export default function Landing({user}) {
  return (
    <>
      <main>
      <div className="relative flex min-h-screen flex-col items-center justify-center overflow-hidden py-6 sm:py-12 bg-white">
      <div className="max-w-xl px-5 text-center">
        <h2 className="mb-2 text-4xl font-bold text-zinc-800">You are not authorized to view the dashboard</h2>
        <p className="mb-2 text-lg text-zinc-500"><span className="font-medium text-indigo-500"></span>.</p>
        <a href="/api/auth/logout" className="bg-emerald-500 my-4 text-white active:bg-emerald-600 font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150">Log out</a>
      </div>
    </div>
      </main>
      <Footer />
    </>
  );
}
