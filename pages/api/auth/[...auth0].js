import { handleAuth, handleLogin } from "@auth0/nextjs-auth0";

export default handleAuth({
    login: handleLogin({
        authorizationParams: {
            audience: 'http://localhost:9090',
            scope: "read:policies read:data read:partner openid profile email"
        }
    })
});