import {getAccessToken, withApiAuthRequired, getSession } from '@auth0/nextjs-auth0'
import axios from 'axios';

export default withApiAuthRequired(async function handler(req, res) {
  const { accessToken } = await getAccessToken(req, res);
  const session = await getSession(req, res);
  const headers = {
    "Authorization": `Bearer ${accessToken}`,
    "x-id-token": session?.idToken,
    'Content-Type': 'application/json'
  }

  try {
    const response = await axios.get('http://localhost:3111/v1/portal/user/info', {
      headers
    })
    const data = response.data;
    return res.status(200).json(data);
  } catch (error) {
    const err = error
    const message = error.message;
    console.error("Error from partner: ", err.response);
    res.status(err.response?.status || 500).json(err?.response?.data);
  }
})