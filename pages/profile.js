import React, { useEffect, useState } from "react";

export default function Profile( props ) {
    console.log(props.user);
    const [userData, setUserData] = useState(null);

    useEffect(() => {
        async function fetchData() {
            const response = await fetch('/api/auth/getdata', { method: "GET"});
            const json = await response.json();
            console.log(json);
        }

        fetchData();
    }, [setUserData])

    return (
        props.user ? (
            <div>
                <a href='api/auth/logout'>LOGOUT</a>
                <img src={props.user.picture } alt={ props.user.name } />
                <h2 className="text-3xl font-bold underline">{ props.user.name }</h2>
                <p>{ props.user.email }</p>
            </div>
        ) : <></>
    ) 
}