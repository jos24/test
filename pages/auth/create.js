import React, { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router"

// layout for page
// c0fa1bc00531bd78ef38c628449c5102aeabd49b5dc3a2a516ea6ea959d6658e
import Auth from "layouts/Auth.js";

export default function Login() {
    const [options, setOptions] = useState([]);
    const [roles, setRoles] = useState([]);
    const [selectedValues, setSelectedValues] = useState([]);
    const [subdomain, setSubdomain] = useState('');
    const [email, setEmail] = useState('');
    // const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [isCreated, setIsCreated] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const router = useRouter();

    useEffect(() => {
        // fetch options from BE

        fetchOptions()
        fetchRoles()
    },  [])

    const fetchOptions = async () => {
        try {
            const response = await fetch('http://localhost:3111/v1/portal/partners');
            const json = await response.json();
            setOptions(json.data)
        } catch (error) {
            console.error('Error fetching options:', error);
        }
    }

    const fetchRoles = async () => {
        try {
            const response = await fetch('http://localhost:3111/v1/portal/roles');
            const json = await response.json();
            console.log("Roles: ", json.data);
            setRoles(json.data)
        } catch (error) {
            console.error('Error fetching roles:', error);
        }
    }

    const handleOptionChange = (event) => {
        console.log("subdomain: ", event.target.value)
        setSubdomain(event.target.value);
      };

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };

    // const handlePasswordChange = (event) => {
    //     setPassword(event.target.value);
    // };

    const handleNameChange = (event) => {
        setName(event.target.value);
    };

    const handleCheckboxChange = (event) => {
        const value = event.target.value;
        const isChecked = event.target.checked;
    
        if (isChecked) {
            console.log("KKK: ", value, isChecked);
          setSelectedValues((prevValues) => [...prevValues, value]);
        } else {
            console.log("un: ", value, isChecked);
          setSelectedValues((prevValues) => prevValues.filter((v) => v !== value));
        }
      };

    const handleSubmit = (event) => {
        event.preventDefault();
        sendToBackend();
    };

    const handleModal = (event) => {
        event.preventDefault();
        if (isCreated) router.push('/api/auth/login')
        else {
            setIsCreated(false);
            setErrorMessage('')
        }
    };
    
    
    const sendToBackend = () => {
    // Make an API call to send the selected value to the backend
    // Replace 'YOUR_API_ENDPOINT' with the actual endpoint URL
    console.log("Balb: ", selectedValues);
        fetch('http://localhost:3111/v1/portal/users', {
            method: 'POST',
            body: JSON.stringify({ partnerSubdomain: subdomain, email, name, roles: selectedValues }),
            headers: {
            'Content-Type': 'application/json',
            },
        })
        .then((response) => response.json())
        .then((data) => {
            // Handle the response from the backend if needed
            console.log('Response from backend:', data);
            if (data.status === "error") {
                setErrorMessage(data.error.message ?? data.message);
                setIsCreated(false);
            } else setIsCreated(true);
        })
        .catch((error) => {
            console.error('Error sending data to backend:', error);
        });
    };

    if (isCreated || errorMessage.length > 0) {
        return (
                <main className="flex justify-center item-center">
                  <div
                    className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                    <div className="relative w-auto my-6 mx-auto max-w-sm">
                      {/*content*/}
                      <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                        {/*header*/}
                        <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                          <h3 className="text-l font-semibold">
                          { errorMessage.length === 0 ? "Account Created Successfully" : "Account Creation Failed" }
                          </h3>
                        </div>
                        {/*body*/}
                        <div className="relative p-6 flex-auto">
                            {/* <h3 className="text-base px-4 font-semibold leading-6 text-gray-900" id="modal-title">{ errorMessage.length === 0 ? "Account Created Successfully" : "Account Creation Failed" }</h3> */}
                            <div className="mt-2">
                                <p className="text-sm px-4 my-4 text-gray-500">{ errorMessage.length === 0 ? "An email has been sent to the user to notify them of their account creation. They will be prompted to change their password." : errorMessage }</p>
                            </div>
                        </div>
                        {/*footer*/}
                        <div className="flex items-center mx-4 justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                            {
                                errorMessage.length > 0 ?
                                    <button
                                        className="text-red-500 background-transparent font-bold px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                        type="button"
                                        onClick={handleModal}>
                                        Retry
                                    </button>
                                : <button
                                    className="bg-emerald-500 my-4 text-white active:bg-emerald-600 font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                    type="button"
                                    onClick={handleModal}>
                                    Back To Dashboard
                                </button>
                            }
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
            </main>
          );
    }

  return (
    <>
      <div className="container mx-auto px-4 h-full">
        <div className="flex content-center items-center justify-center h-full">
          <div className="w-full lg:w-4/12 px-4">
            <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0">
              <div className="rounded-t mb-0 px-6 py-6">
                <div className="text-center mb-3">
                  <h6 className="text-blueGray-500 text-lg font-bold">
                    User Creation
                  </h6>
                </div>
                <hr className="mt-6 border-b-1 border-blueGray-300" />
              </div>
              <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
                <form className="space-y-6" action="#" method="POST" onSubmit={handleSubmit} noValidate>
                    <div className="relative w-full mb-3">
                        <label
                        className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                        htmlFor="grid-password"
                        >
                        Full Name
                        </label>
                        <input onChange={handleNameChange} required type="text" className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" placeholder="Full Name"
                        />
                    </div>

                    <div className="relative w-full mb-3">
                        <label
                        className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                        htmlFor="grid-password"
                        >
                        Email
                        </label>
                        <input
                            onChange={handleEmailChange}
                        type="email"
                        required
                        className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                        placeholder="Email"
                        />
                    </div>

                    {/* <div className="relative w-full mb-3">
                        <label
                        className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                        htmlFor="grid-password"
                        >
                        Password
                        </label>
                        <input
                        onChange={handlePasswordChange}
                        type="password"
                        required
                        className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                        placeholder="Password"
                        />
                    </div> */}
                    <div className="relative w-full mb-3">
                        <label
                        className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                        htmlFor="grid-password"
                        >
                        Partner
                        </label>
                        <select
                        onChange={handleOptionChange}
                        type="password"
                        required
                        className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                        placeholder="Password">
                            <option value="">Select Partner</option>
                                    {
                                        options.map((option) => (
                                        <option className='sm:text-sm sm:leading-6 text-gray-900' key={option._id} value={option.subdomain}>
                                            { option.name }
                                        </option>
                                    ))
                                }
                        </select>
                    </div>
                    <div className="relative w-full mb-3">
                        <label
                        className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                        htmlFor="grid-password"
                        >
                            Roles
                        </label>
                            {
                                roles.map((role) => (
                                    <div className="flex items-center mb-4">
                                        <input onChange={handleCheckboxChange} checked={selectedValues.includes(role.id)} id={role.id} type="checkbox" value={role.id} className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                                        <label key={role.id} htmlFor={role.id} className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">{`${role.name} (${role.description})`}</label>
                                    </div>
                                ))
                            }  
                        
                        {/* <div className="flex items-center mb-4">
                            <input id="default-checkbox" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                            <label htmlFor="default-checkbox" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Default checkbox</label>
                        </div> */}
                    </div>
                    <div className="text-center mt-6">
                        <button
                        className="bg-blueGray-800 text-white active:bg-blueGray-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
                        type="button" onClick={handleSubmit}
                        >
                        Create User
                        </button>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

Login.layout = Auth;