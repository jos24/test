import React, { useState, useEffect } from "react";

// components

import CardTable from "components/Cards/CardTable.js";

// layout for page

import Admin from "layouts/Admin.js";
import UserContext from "context/user";
import router from "next/router";




export default function Tables(props) {
  console.log("User: ", props.user);
  const [userData, setUserData] = useState(null);

  useEffect(() => {
      async function fetchData() {
        try {
          const response = await fetch('/api/auth/getdata', { method: "GET"});
          const json = await response.json();
          console.log("json: ", json);
          if (json.statusCode === 400) alert("Oops, error fetching dashboard data. Try again")
          if (json.statusCode === 403) return router.push('/landing')
          setUserData(json.data);
        } catch (error) {
          return alert("Server Error")
        }
      }

      fetchData();
  }, [setUserData])
  return (
    <>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <CardTable userData={userData} />
        </div>
        {/* <div className="w-full mb-12 px-4">
          <CardTable color="dark" />
        </div> */}
      </div>
    </>
  );
}

Tables.layout = Admin;
